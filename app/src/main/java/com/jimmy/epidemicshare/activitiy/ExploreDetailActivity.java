package com.jimmy.epidemicshare.activitiy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.jimmy.epidemicshare.R;
import com.jimmy.epidemicshare.adapter.DiseaseImageAdapter;
import com.jimmy.epidemicshare.api.API;
import com.jimmy.epidemicshare.api.ServiceGenerator;
import com.jimmy.epidemicshare.callbacks.OnActionbarListener;
import com.jimmy.epidemicshare.model.DeletePenyakitResponse;
import com.jimmy.epidemicshare.model.DiseaseModel;
import com.jimmy.epidemicshare.model.MultimediaBody;
import com.jimmy.epidemicshare.model.MultimediaPostResponse;
import com.jimmy.epidemicshare.utils.FunctionUtil;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;


public class ExploreDetailActivity extends BaseActivity {

    private int SELECT_FILE = 1;
    private Toolbar toolbar;

    private TextView detailTxtUserName;
    private TextView detailTxtDiseaseName;
    private TextView detailTxtType;
    private TextView detailTxtNumber;
    private TextView detailTxtDescription;
    private TextView detailTxtSympton;
    private TextView detailTxtSuggest;

    private GridView pictureList;

    private TextView btnDelete;

    private RelativeLayout btnGetMap;

    private DiseaseImageAdapter adapter;

    private ArrayList<String> diseaseImageUrls;

    public DiseaseModel diseaseData;

    private API api;

    private String image_name="";
    private String image_string = "";
    private String id_user = "";

    private SharedPreferences sharedPreferences;
    private final String name = "loginUser";
    private static final int mode = Activity.MODE_PRIVATE;






    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ButterKnife.bind(this);
        adapter = new DiseaseImageAdapter(this);
        diseaseImageUrls = new ArrayList<>();

        Intent i = getIntent();
        Bundle b = i.getExtras();


        diseaseData = (DiseaseModel) i.getSerializableExtra("disease");

        api = ServiceGenerator.createService(API.class);
        sharedPreferences = getSharedPreferences(name, mode);
        id_user = sharedPreferences.getString("id_user","");

        fetchData();
        setRightIcon(0);
        setActionBarTitleCenter("Epidemic Share");
    }

    @Override
    public void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.setBackgroundColor(getResources().getColor(R.color.actionbar_color));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        detailTxtUserName = (TextView) findViewById(R.id.disease_detail_uploader_name);
        detailTxtDiseaseName = (TextView) findViewById(R.id.disease_detail_name);
        detailTxtType = (TextView) findViewById(R.id.disease_detail_type);
        detailTxtNumber = (TextView) findViewById(R.id.disease_detail_number);
        detailTxtDescription = (TextView) findViewById(R.id.disease_detail_desc);
        detailTxtSympton = (TextView) findViewById(R.id.disease_detail_gejala);
        detailTxtSuggest = (TextView) findViewById(R.id.disease_detail_saran);

        pictureList = (GridView) findViewById(R.id.disease_image_list);


        btnDelete = (TextView) findViewById(R.id.btn_delete);
        btnGetMap = (RelativeLayout) findViewById(R.id.get_map_btn);

        int deviceWidth = FunctionUtil.getDeviceSize(this).x;
        RelativeLayout.LayoutParams gridViewParams = (RelativeLayout.LayoutParams) pictureList.getLayoutParams();
        gridViewParams.width = (deviceWidth / 3) * 2;
        gridViewParams.height = deviceWidth;
        gridViewParams.topMargin = -(gridViewParams.width / 4);
        gridViewParams.bottomMargin = gridViewParams.topMargin;
        gridViewParams.leftMargin = -(gridViewParams.topMargin);
        pictureList.setLayoutParams(gridViewParams);

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                share();
               /* if(id_user.equalsIgnoreCase(diseaseData.getId_user_app().toString())) {
                    deletePenyakit(v);
                }
                else
                {
                    Toast.makeText(ExploreDetailActivity.this, "Anda tidak dapat menghapus penyakit yang bukan anda buat", Toast.LENGTH_SHORT).show();
                }*/
            }
        });

    }

    public void share()
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Nama Penyakit : "+diseaseData.getNama_penyakit()+"\n\n"
                +"Tipe Penyakit : "+diseaseData.getNama_tipe_penyakit()+"\n\n"
                +"Jumlah Penderita : "+diseaseData.getJumlah_penderita()+"\n\n"
                +"Deskripsi : \n"+diseaseData.getDeskripsi()+"\n\n"
                +"Gejala : \n"+diseaseData.getGejala()+"\n\n"
                +"Saran Penanganan : \n"+diseaseData.getSaran_penanganan());
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
    private void deletePenyakit(View v) {
        final ProgressDialog dialog = new ProgressDialog(v.getContext());
        dialog.setMessage("Please wait...");
        dialog.show();

        Call<DeletePenyakitResponse> call = null;
        call = api.deletePenyakit(diseaseData.getId_penyakit());
        call.enqueue(new Callback<DeletePenyakitResponse>() {
            @Override
            public void onResponse(Response<DeletePenyakitResponse> response, Retrofit retrofit) {
                dialog.dismiss();
                if (2 == response.code() / 100) {
                    finish();
                } else {
                    Toast.makeText(ExploreDetailActivity.this, "Error delete penyakit", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                dialog.dismiss();
                Toast.makeText(ExploreDetailActivity.this, "Failed delete penyakit", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void setUICallbacks() {
        setActionbarListener(new OnActionbarListener() {
            @Override
            public void onLeftIconClick() {
                onBackPressed();
            }

            @Override
            public void onRightIconClick() {

                share();
            }
        });
    }

    @Override
    public int getLayout() {
        return R.layout.activity_explore_detail;
    }

    @Override
    public void updateUI() {

        adapter.setData(diseaseImageUrls);
        pictureList.setAdapter(adapter);


    }

    public void fetchData() {

        detailTxtUserName.setText(diseaseData.getNama());
        detailTxtDiseaseName.setText(diseaseData.getNama_penyakit());
        detailTxtType.setText(diseaseData.getNama_tipe_penyakit());
        detailTxtNumber.setText(diseaseData.getJumlah_penderita());
        detailTxtDescription.setText(diseaseData.getDeskripsi());
        detailTxtSympton.setText(diseaseData.getGejala());
        detailTxtSuggest.setText(diseaseData.getSaran_penanganan());
        for (int i = 0; i < diseaseData.getMultimedia().size(); i++) {
            diseaseImageUrls.add(diseaseData.getMultimedia().get(i).getUrl());
        }

        adapter.notifyDataSetChanged();
    }




}
