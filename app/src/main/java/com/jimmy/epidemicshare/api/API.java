package com.jimmy.epidemicshare.api;


import com.jimmy.epidemicshare.R;
import com.jimmy.epidemicshare.model.DeletePenyakitResponse;
import com.jimmy.epidemicshare.model.DiseaseBody;
import com.jimmy.epidemicshare.model.DiseasePostResponse;
import com.jimmy.epidemicshare.model.DiseaseResponse;
import com.jimmy.epidemicshare.model.MultimediaBody;
import com.jimmy.epidemicshare.model.MultimediaPostResponse;
import com.jimmy.epidemicshare.model.PutPenyakitBody;
import com.jimmy.epidemicshare.model.PutPenyakitResponse;
import com.jimmy.epidemicshare.model.TipePenyakitResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by DedeEko on 5/18/2016.
 */
public interface API {

    //Penyakit
    @GET("publics/penyakitall")
    Call<DiseaseResponse>
    getDiseaseList(@Query("api_key") String api_key);

    @GET("publics/penyakit")
    Call<DiseaseResponse>
    getDiseaseListByLokasi(@Query("api_key") String api_key, @Query("id_desa") String id_desa);

    @POST("penyakit")
    Call<DiseasePostResponse>
    postDisease(@Body DiseaseBody diseaseBody);
    @DELETE("penyakit")
    Call<DeletePenyakitResponse> deletePenyakit(@Query("Id_penyakit") String id_penyakit);

    @PUT("penyakit")
    Call<PutPenyakitResponse> editPenyakit(@Query("Id_penyakit") String id_penyakit,
                                           @Body PutPenyakitBody putPenyakitBody);

    @GET("tipepenyakit/{page}/{count_page}")
    Call<TipePenyakitResponse>
    getTipePenyakitList(@Path("page") String page, @Path("count_page") String count_page);

    @POST("multimedia")
    Call<MultimediaPostResponse>
    postMultimedia(@Body MultimediaBody multimediaBody);
}
