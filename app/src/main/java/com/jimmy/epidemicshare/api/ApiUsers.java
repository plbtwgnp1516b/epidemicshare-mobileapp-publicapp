package com.jimmy.epidemicshare.api;


import com.jimmy.epidemicshare.model.LoginResponse;
import com.jimmy.epidemicshare.model.SignupBody;
import com.jimmy.epidemicshare.model.SignupPostResponse;

import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

public interface ApiUsers {



    @POST("users")
    Call<SignupPostResponse> postSignup(@Body SignupBody signupBody);

    @GET("users/1/1000")
    Call<LoginResponse> cekLogin(@Query("username") String username, @Query("password") String password);

    @GET("users/1/1000")
    Call<LoginResponse> getUser(@Query("username") String username, @Query("password") String password);
}
