package com.jimmy.epidemicshare.callbacks;

public interface OnActionbarListener {

    public void onLeftIconClick();
    public void onRightIconClick();
}
